const routes = require('express').Router();
const userRoutes = require('../src/user/userRoutes');

routes.use('/user', userRoutes);


routes.get('/', (req, res) => {
    res.status(200).json(
        {
            message: 'Connected!'
        }
    );
});


module.exports = routes;