const mongoose = require('mongoose');
let Schema = mongoose.Schema;

var userSchema = new Schema({
    userId: { type: String, required: true, unique: true},
    name: String,
    address: String
});

var User = mongoose.model('users', userSchema);

module.exports = User;