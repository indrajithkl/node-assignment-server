const routes = require('express').Router();

const userController = require('./userController');

routes.post('/', userController.create); // Create User
routes.get('/', userController.list); // List of Users
routes.get('/search', userController.search); //Search Users
routes.get('/sort', userController.sort); //Sort Users Expects an mandatory field sort_field and optional field sort_order - 'asc' || 'desc'
routes.get('/:userId', userController.get); //Get Details of a User
routes.put('/:userId', userController.update); // Update a User
routes.delete('/delete/:userId', userController.delete); // Delete a User

module.exports = routes;
