const User = require('./userModel');


var createUser = (req, res) => {
    let user = new User({
        userId: req.body.userId,
        name: req.body.name,
        address: req.body.address
    });

    user.save((err) => {
        if (err && err.errmsg) {
            console.log(err);
            res.send({
                error: err.errmsg
            });
        } else {
            res.send({
                success: 'User Created'
            });
        }
    });
};

var getUser = (req, res) => {
    let userId = req.params.userId;
    User.findOne({
        userId: userId
    }, (error, response) => {
        if (error) res.status(500).send({
            error: 'DB Error'
        });
        else {
            if (response) {
                res.status(200).send(response);
            } else {
                res.status(404).send({
                    error: 'User Not Found '
                });
            }
        }
    })

}

var listUser = (req, res) => {
    let userId = req.params.userId;
    if (!userId) {
        User.find({}, (error, response) => {
            if (error) res.status(500).send({
                error: 'DB Error'
            });
            else {
                if (response) {
                    res.status(200).send(response);
                } else {
                    res.status(404).send({
                        error: 'User Not Found '
                    });
                }
            }
        });
    }

}


var deleteUser = (req, res) => {
    let userId = req.params.userId;
    User.findOneAndRemove({
        userId: userId
    }, (error, response) => {
        if (error) res.status(500).send({
            error: error
        });
        else {
            if (response) {
                if (response.userId) {
                    res.status(200).send({
                        success: 'User Removed'
                    });
                } else {
                    res.status(401).send({
                        error: 'Unknown error'
                    });
                }

            } else {
                res.status(404).send({
                    error: 'User Not Found '
                });
            }
        }
    });
}

var updateUser = (req, res) => {
    let userId = req.params.userId;
    console.log(req.body);
    const updateData = {
        name: req.body.name,
        address: req.body.address
    }
    User.findOneAndUpdate({
        userId: userId
    }, updateData, {
        upsert: false
    }, (error, response) => {
        if (error) res.status(500).send({
            error: error
        });
        else {
            if (response) {
                // console.log(response);
                res.status(200).send({
                    success: 'User Updated'
                });
            } else {
                res.status(404).send({
                    error: 'User Not Found'
                });
            }
        }
    });

}

var searchUser = (req, res) => {
    let searchQuery = req.query;
    if (searchQuery.name) {
        searchQuery.name = new RegExp(searchQuery.name, "i");
    }
    if (searchQuery.address) {
        searchQuery.address = new RegExp(searchQuery.address, "i");
    }



    User.find(searchQuery, (error, response) => {
        if (error) res.status(500).send({
            error: 'DB Error'
        });
        else {
            if (response) {
                res.status(200).send(response);
            } else {
                res.status(404).send({
                    error: 'User Not Found '
                });
            }
        }
    });

}

var sortUser = (req, res) => { 
    let sortField = null;
    let sortOrder = 'asc';
    if (!req.query.hasOwnProperty('sort_field')) {
        res.status(400).send({ error: 'sort_field is required' });
        return;
    } else {
        sortField = req.query.sort_field;
    }

    if (req.query.hasOwnProperty('sort_order')) {
        sortOrder = req.query.sort_order;
    }

    const sortQuery = {

    }

    sortQuery[sortField] = sortOrder;

    
    User.find({}).sort(sortQuery).exec((err, response) => {
        if(err)  res.status(500).send({
            error: error
        });
        res.status(200).send(response);
    });    


}

module.exports = {
    create: createUser,
    get: getUser,
    list: listUser,
    delete: deleteUser,
    update: updateUser,
    search: searchUser,
    sort: sortUser
};