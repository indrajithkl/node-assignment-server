const app = require('express')();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const routes = require('./routes');
const PORT = 3000;

app.use(bodyParser.urlencoded({extended:true}));

mongoose.connect('mongodb://localhost/user_db', {
    useMongoClient:true
});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function(){
    console.log('conncetion opened')
    app.use('/',routes);
});
app.listen(PORT, ()=>{
    console.log(`App listening at port ${PORT}`);
});